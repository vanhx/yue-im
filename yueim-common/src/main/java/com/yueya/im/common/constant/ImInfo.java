package com.yueya.im.common.constant;

public class ImInfo {
    public static final String USER_PREFIX = "userActor_";
    public static final String ROUTE_PREFIX = "/user/";
    public static final String SEND_ROUTE= "SendRoute";
    public static final String REVIVE_ROUTE= "ReciveRoute";
    public static final String SYSTEM_NAME = "YueImClusterSystem";
    public static final String MODE_SINGLE = "single";
    public static final String MODE_CLUSTER = "cluster";
    /**
     * 路由节点默认容量
     */
    public static final int INIT_SIZE = 400000;
}
