package com.yueya.im.server;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.yueya.im.common.api.ImSetting;
import com.yueya.im.server.actors.ImActorSystem;
import com.yueya.im.server.netty.NettyServer;

public class ClusterNodeApp {
    private static Config root;
    public static void start(String conf, ImSetting setting){
        root = ConfigFactory.load(conf);
        ImActorSystem.getInstance().init(root,setting.getListener());
        NettyServer server = new NettyServer();
        int port = root.getConfig("akka").getConfig("netty").getInt("port");
        String mode = root.getConfig("akka").getConfig("netty").getString("mode");
        server.start(setting.getHandlerMap(),setting.getBusinessInfoProvider(),port,mode);
    }

}
