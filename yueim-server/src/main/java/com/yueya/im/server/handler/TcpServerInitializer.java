package com.yueya.im.server.handler;


import com.yueya.im.common.api.BusinessInfoProvider;
import com.yueya.im.common.api.CmdHandler;
import com.yueya.im.server.actors.TcpDispatcher;
import com.yueya.im.server.base.TcpSender;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

import java.util.concurrent.ConcurrentHashMap;

public class TcpServerInitializer extends ChannelInitializer<SocketChannel> {

    private TcpDispatcher tcpDispatcher = new TcpDispatcher();
    @Override
    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new TcpMsgDecoder());
        TcpHandler tcpHandler = new TcpHandler();
        TcpSender tcpSender = new TcpSender();
        tcpDispatcher.setSender(tcpSender);
        tcpHandler.setDispatcher(tcpDispatcher);
        pipeline.addLast(tcpHandler);
    }

    public void setHandlers(ConcurrentHashMap<String, CmdHandler> handlerMap, BusinessInfoProvider provider) {
        tcpDispatcher.setHandlers(handlerMap);
        tcpDispatcher.setProvider(provider);
    }
}
