package com.yueya.im.server.handler;

import com.yueya.im.common.api.BusinessInfoProvider;
import com.yueya.im.common.api.CmdHandler;
import com.yueya.im.common.api.MsgSender;
import com.yueya.im.server.actors.WebSocketDispatcher;
import com.yueya.im.server.base.DefaultSender;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.util.concurrent.DefaultEventExecutorGroup;
import io.netty.util.concurrent.EventExecutorGroup;

import java.util.concurrent.ConcurrentHashMap;

public class WebSocketServerInitializer extends ChannelInitializer<SocketChannel> {

        private WebSocketDispatcher webSocketDispatcher = new WebSocketDispatcher();
        private EventExecutorGroup executors = new DefaultEventExecutorGroup(16);
        @Override
        public void initChannel(SocketChannel ch) throws Exception {
            ChannelPipeline pipeline = ch.pipeline();
            pipeline.addLast(new HttpServerCodec());
            pipeline.addLast(new HttpObjectAggregator(65536));
            pipeline.addLast(new WebSocketServerCompressionHandler());
            WebSocketServerHandler handler = new WebSocketServerHandler();
            MsgSender msgSender = new DefaultSender();
            webSocketDispatcher.setSender(msgSender);
            handler.setDispatcher(webSocketDispatcher);
            pipeline.addLast(executors,handler);
        }

        public void setHandlers(ConcurrentHashMap<String, CmdHandler> handlerMap, BusinessInfoProvider provider) {
            webSocketDispatcher.setHandlers(handlerMap);
            webSocketDispatcher.setProvider(provider);
        }
}
